import QtQuick
import QtQuick.Effects

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("GrayImage")

    Image {
        source: 'assets/pvz-wall-nut.png'

        layer.enabled: true
        layer.effect: MultiEffect {
            saturation: -1
        }
    }
}
