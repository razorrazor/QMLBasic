import QtQuick

// https://code.qt.io/cgit/qt/qtdeclarative.git/tree/examples/quick/imageelements/ShadowRectangle.qml?h=6.8
Item {
    property alias color: rectangle.color

    BorderImage {
        anchors.fill: rectangle
        anchors {
            leftMargin: -6
            topMargin: -6
            rightMargin: -8
            bottomMargin: -8
        }
        border {
            left: 10
            top: 10
            right: 10
            bottom: 10
        }
        source: 'assets/shadow.png'
    }

    Rectangle {
        id: rectangle

        anchors.fill: parent
    }
}
